import sys
import requests
from bs4 import BeautifulSoup
import pprint


def sort_stories_by_votes(hn):
    return sorted(hn, key=lambda x: x['votes'], reverse=True)


def create_custom_hn(links, subtext, minPoints):
    hn = []
    for index, item in enumerate(links):
        title = item.getText()
        href = item.get('href', None)
        vote = subtext[index].select('.score')
        if len(vote):
            points = int(vote[0].getText().replace(' points', ''))
            if points >= minPoints:
                hn.append({'title': title, 'link': href, 'votes': points})
    return hn


def get_custom_hn_for_page(pagenumber, minPoints):
    url = f'https://news.ycombinator.com/news?p={pagenumber}'
    res = requests.get(url)

    soup = BeautifulSoup(res.text, 'html.parser')
    links = soup.select('.storylink')  # select by class
    votes = soup.select('.score')
    subtext = soup.select('.subtext')

    hn = create_custom_hn(links, subtext, minPoints)
    return hn


if __name__ == '__main__':
    try:
        number_of_pages = int(sys.argv[1])
    except:
        number_of_pages = 1

    try:
        min_points = int(sys.argv[2])
    except:
        min_points = 0

    dict_articles = []
    for page in range(number_of_pages):
        dict_articles.extend(
            get_custom_hn_for_page(page+1, min_points))
        print(f'{page}: ', dict_articles)

    dict_articles = sort_stories_by_votes(dict_articles)

    pprint.pprint(dict_articles)
